package fr.naerion.ciexamplesimple.ms.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/myController")
public class MyController {

    @GetMapping
    public String hello() {
        System.out.println("tioto");
        return "hello";
    }

    @GetMapping("hello1")
    public String hello1() {
        System.out.println("tioto");
        return "hello1";
    }
}
