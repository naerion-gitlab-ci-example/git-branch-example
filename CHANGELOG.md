# [1.1.0](https://gitlab.com/naerion-gitlab-ci-example/git-branch-example/compare/v1.0.0...v1.1.0) (2019-12-06)


### Features

* feat ([7b0e52a](https://gitlab.com/naerion-gitlab-ci-example/git-branch-example/commit/7b0e52ab99d16aeec2b805a5bd4d40b487f55056))

# 1.0.0 (2019-12-04)


### Features

* add new Hello endpoint ([de70fc1](https://gitlab.com/naerion-gitlab-ci-example/git-branch-example/commit/de70fc18dc1496cba87efc11d8cfea787f7d916a))
