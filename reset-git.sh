# Remove the history from 
rm -rf .git
rm -rf CHANGELOG.md
jq '.version = "0.0.0"' package.json | sponge package.json

mkdir -p override/main
mkdir -p override/test
mv ms/src/main/java/fr/naerion/ciexamplesimple/ms/controller/MyController.java override/main
mv ms/src/test/java/fr/naerion/ciexamplesimple/ms/controller/MyControllerTest.java override/test
# recreate the repos from the current content only
git init
git add .
git commit -m "Initial commit"

# push to the github remote repos ensuring you overwrite history
git remote add origin https://gitlab.com/naerion-gitlab-ci-example/git-branch-example.git
git push -u --force origin master
